from flask import Flask, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, abort
from functools import wraps
from bson import ObjectId
import json
import pymongo
import datetime
import jwt

app = Flask(__name__)
api = Api(app)

# Connect to the database
dbconn = pymongo.MongoClient("mongodb://localhost:27017/")
db = dbconn['log']
log_collection = db["log_table"]


class JSONEncoder(json.JSONEncoder):
   def default(self, obj):
       if isinstance(obj, ObjectId):
           return str(obj)
       return json.JSONEncoder.default(self, obj)


class Return(Resource):
    def get(self):
        logs = [i for i in log_collection.find()]
        return json.dumps(logs, cls=JSONEncoder), 200


# Authentication pending


class Logger(Resource):
   # method_decorators = {'post': [token_required]}

   # GET endpoint.
   def get(self):
       return "OK",200

   # Validates the string, extracts the data and stores it in a database
   def post(self):
       parser = reqparse.RequestParser()
       parser.add_argument("uk")
       parser.add_argument("hub")
       parser.add_argument("log_string")
       args = parser.parse_args()

       log_data = {
           "time": str(datetime.datetime.now()),
           "uk": args["uk"],
           "hub": args["hub"],
           "log": args["log_string"]
       }

       x = log_collection.insert_one(log_data)
       if x.acknowledged == True:
           return json.dumps(log_data, cls=JSONEncoder), 201
       else:
           return "Error", 404


##
# Setting up the resource urls here
##
api.add_resource(Logger, "/log/")
api.add_resource(Return, "/logs/")
app.run(host="0.0.0.0", port=3000, debug=True)
